<?php include "inc/header.php";
include "inc/sidebar.php"; 


// get page id

if (!isset($_GET['pageid']) || $_GET['pageid'] == NULL) {
    echo "<script>window.location = 'index.php';</script>";
}else{
    $id = $_GET['pageid'];
}

// add post


if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {

    $name = $fm->validation($_POST['name']);
    $body = $fm->validation($_POST['body']);

    $name = mysqli_real_escape_string($db->link, $name);
    $body = mysqli_real_escape_string($db->link, $body);

    if ($name == "" || $body == "") {
        echo "<span class='error'>Field must no empty!</span>";
    }else{

        $query = "UPDATE tbl_page SET 
        name = '$name',
        body = '$body'
        WHERE id = '$id'";

        $inserted_rows = $db->insert($query);
        if ($inserted_rows) {
         echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Inserted Successfully.
         </span></div>";
     }else {
         echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Not Inserted Successfully.
         </span></div>";
     }
 }
}
?>

<div class="grid_10">

    <div class="box round first grid">
        <h2>Add New Post</h2>
        <div class="block">
           <?php $sql = "SELECT * FROM tbl_page WHERE id = '$id'";
           $posts = $db->select($sql);
           if ($posts) {
            while($result = $posts->fetch_assoc()){ ?>              
                <form action="" method="POST">
                    <table class="form">
                        <tr>
                            <td>
                                <label>Name</label>
                            </td>
                            <td>
                                <input type="text" name="name" value="<?php echo $result['name']; ?>" class="medium" />
                            </td>
                        </tr>

                        <tr>
                            <td style="vertical-align: top; padding-top: 9px;">
                                <label>Content</label>
                            </td>
                            <td>
                                <textarea name="body" class="tinymce">
                                    <?php echo $result['body']; ?>
                                </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Update" class="btn btn-primary" />
                                <span ><a class="actiondel btn btn-danger" href="delpage.php?delid=<?php echo $result['id']; ?>">Delete</a></span>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </form>
            <?php   }
        }
        ?>
    </div>
</div>
</div>

<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php"; ?>

</body>
</html>

