﻿<?php include "inc/header.php";
include "inc/sidebar.php"; 


if (isset($_GET['seenid'])) {
	$seenid = $_GET['seenid'];
	$sql = "UPDATE tbl_contact SET 
	status = '1'
	WHERE id = '$seenid'
	";

	$msglist = $db->update($sql);
	if ($msglist) {
		echo "<div style='text-align:center;'><span class='btn bg-success;'>Message sent in the Seen box.</span></div>";
	}else{
		echo "<div style='text-align:center;'><span class='btn bg-success;'>Message does not send.</span></div>";
	}
}
?>
<div class="grid_10">
	<div class="box round first grid">
		<h2>Inbox</h2>
		<div class="block">        =
			<table class="data display datatable" id="example">
				<thead>
					<tr>
						<th>Serial No.</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Body</th>
						<th>Date</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$sql = "SELECT * FROM tbl_contact WHERE status = '0'";
					$contlist = $db->select($sql);
					if ($contlist) {
						while ($result = $contlist->fetch_assoc()) { ?>
							<tr class="odd gradeX">
								<td width="5%"><?php echo $result['id'] ?></td>
								<td width="10%"><?php echo $result['firstname'] ?></td>
								<td width="10%"><?php echo $result['lastname'] ?></td>
								<td width="15%"><?php echo $result['email'] ?></td>
								<td width="25%"><?php echo $fm->textShorten($result['body'], 35) ?></td>
								<td width="20%"><?php echo $fm->dateFormat($result['date']) ?></td>
								<td width="15%"><a href="viewmsg.php?msgid=<?php echo $result['id']; ?>">Edit</a> ||
									<a href="replaymsg.php?msgid=<?php echo $result['id']; ?>">Replay</a>||
									<a onclick="return confirm('are you sure to move the seen box!')" href="?seenid=<?php echo $result['id']; ?>">Seen</a>
								</td>
							</tr>
						<?php }} ?>
					</tbody>
				</table>
			</div>
		</div>




		<?php 

///

		if (isset($_GET['unseenid'])) {
			$unseenid = $_GET['unseenid'];
			$sql = "UPDATE tbl_contact SET 
			status = '0'
			WHERE id = '$unseenid'
			";

			$unseenmsg = $db->update($sql);
			if ($unseenmsg) {
				echo "<div style='text-align:center;'><span class='btn bg-success;'>Message sent in the Seen box.</span></div>";
			}else{
				echo "<div style='text-align:center;'><span class='btn bg-success;'>Message does not send.</span></div>";
			}
		}

/// Delete message


		if (isset($_GET['deleteid'])) {
			$deleteid = $_GET['deleteid'];
			$sql = "DELETE FROM tbl_contact WHERE id = '$deleteid'";
			$delmsg = $db->delete($sql);
			if ($delmsg) {
				echo "<div style='text-align:center;'><span class='btn bg-success;'>Message Deleted Successfully.</span></div>";
			}else{
				echo "<div style='text-align:center;'><span class='btn bg-success;'>Message does not Deleted.</span></div>";
			}
		}
		?>

		<div class="box round first grid">
			<h2>Seen Message</h2>
			<div class="block">        
				<table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Body</th>
							<th>Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$sql = "SELECT * FROM tbl_contact WHERE status = '1'";
						$contlist = $db->select($sql);
						if ($contlist) {
							while ($result = $contlist->fetch_assoc()) { ?>
								<tr class="odd gradeX">
									<td width="5%"><?php echo $result['id'] ?></td>
									<td width="10%"><?php echo $result['firstname'] ?></td>
									<td width="10%"><?php echo $result['lastname'] ?></td>
									<td width="15%"><?php echo $result['email'] ?></td>
									<td width="25%"><?php echo $fm->textShorten($result['body'], 35) ?></td>
									<td width="20%"><?php echo $fm->dateFormat($result['date']) ?></td>
									<td width="15%"><a href="viewmsg.php?msgid=<?php echo $result['id']; ?>">Edit</a> ||
										<a onclick="return confirm('are you sure to move the Unseen box!')" href="?unseenid=<?php echo $result['id']; ?>">Unseen</a>||
										<a onclick="return confirm('are you sure to delete the msg!')" href="?deleteid=<?php echo $result['id']; ?>">Delete</a>
									</td>
								</tr>
							<?php }} ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!-- END: load jquery -->
		<script type="text/javascript" src="js/table/table.js"></script>
		<script src="js/setup.js" type="text/javascript"></script>
		<script type="text/javascript">

			$(document).ready(function () {
				setupLeftMenu();

				$('.datatable').dataTable();
				setSidebarHeight();


			});
		</script>

		<?php include "inc/footer.php"; ?>
