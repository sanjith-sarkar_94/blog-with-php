<?php 
include "../lib/Session.php";
Session::init();
Session::checkLogin();
?>
<?php
include "../config/config.php";
include "../lib/Database.php";
include "../helpers/format.php";
$db = new Database();
$fm = new Format();
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="css/stylelogin.css" media="screen" />
</head>
<body>
	<div class="container">
		<section id="content">
			<?php 
			if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['login'])) {
				$username = $fm->validation($_POST['username']);
				$password = $fm->validation(md5($_POST['password']));

				$username = mysqli_real_escape_string($db->link, $username);
				$password = mysqli_real_escape_string($db->link, $password);

				$sql = "SELECT * FROM tbl_user WHERE username = '$username' AND password = '$password'";
				$result = $db->select($sql);
				if ($result == true) {
					$value  = $result->fetch_assoc();

					Session::set("login", true);
					Session::set("username", $value['username']);
					Session::set("userid", $value['id']);
					Session::set("userrole", $value['role']);
					Session::set("loginmsg",  "<div style='text-align:center;'><span class='btn bg-success;'><strong>Success!!</strong> Thank You, You are logged in!!.</span></div>");
					header("Location: index.php");
					
					
				}else{
					echo "<span>Sorry! username or password does not matched!</span>";
				}
			}
			?>

			<form action="" method="post">
				<h1>Admin Login</h1>
				<div>
					<input type="text" placeholder="Username" required="" name="username" required />
				</div>
				<div>
					<input type="password" placeholder="Password" required="" name="password" required />
				</div>
				<div>
					<input type="submit" name="login" value="Login" />
				</div>
			</form><!-- form -->
			<div class="button">
				<a href="forgotpass.php">Change Password!</a>
			</div>
			<div class="button">
				<a href="#">Dynamic Live Project!</a>
			</div>
		</section><!-- content -->
	</div><!-- container -->
</body>
</html>