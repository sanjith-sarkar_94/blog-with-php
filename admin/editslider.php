<?php include "inc/header.php";
include "inc/sidebar.php"; 


// get slideid

if (!isset($_GET['slideid']) || $_GET['slideid'] == NULL){
	echo "<script>window.location = 'slidelist.php';</script>";
}else{
	$slideid = $_GET['slideid'];
}


// update slidelist


if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {
	$title = mysqli_real_escape_string($db->link, $_POST['title']);

	$permited  = array('jpg', 'jpeg', 'png', 'gif');
	$file_name = $_FILES['image']['name'];
	$file_size = $_FILES['image']['size'];
	$file_temp = $_FILES['image']['tmp_name'];

	$div = explode('.', $file_name);
	$file_ext = strtolower(end($div));
	$unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
	$uploaded_image = 'upload/slide/'.$unique_image;


	if ($title == "") {
		echo "<span class='error'>Field must no empty!</span>";
	}else{
		if (!empty($file_name)) {
			if ($file_size >1048567) {
				echo "<span class='error'>Image Size should be less then 1MB!</span>";
			} elseif (in_array($file_ext, $permited) === false) {
				echo "<span class='error'>You can upload only:-"
				.implode(', ', $permited)."</span>";
			} else{
				move_uploaded_file($file_temp, $uploaded_image);
				$query = "UPDATE tbl_slider SET 
				title = '$title',
				image = '$uploaded_image'
				WHERE id = '$slideid'
				";
				$slideupdate = $db->update($query);

				if ($slideupdate) {
					echo "<div style='text-align:center;'><span class='btn bg-success;'>Slider Updated Successfully.
					</span></div>";
				}else {
					echo "<div style='text-align:center;'><span class='btn bg-success;'>Slider Not Updated Successfully.
					</span></div>";
				}
			}
		}
		else{
			$query = "UPDATE tbl_post SET 
			title = '$title'
			WHERE id = '$slideid'
			";
			$slideupdate = $db->update($query);

			if ($slideupdate) {
				echo "<div style='text-align:center;'><span class='btn bg-success;'>Slider Updated Successfully.
				</span></div>";
			}else {
				echo "<div style='text-align:center;'><span class='btn bg-success;'>Slider Not Updated Successfully.
				</span></div>";
			}
		}
	}
}
?>


<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Slider</h2>
        <div class="block copyblock"> 


    <?php 
        $sql = "SELECT * FROM tbl_slider WHERE id = '$slideid'";
        $slidelist = $db->select($sql);
        if ($slidelist) {
            while ($result = $slidelist->fetch_assoc()) { ?>

                <form action="" method="POST" enctype="multipart/form-data">
                 <table class="form">  
                     <tr>
                        <td><label>Serial No:</label></td>
                        <td>
                            <input type="text" name="id" readonly value="<?php echo $result['id']; ?>" class="medium" />
                        </td>
                    </tr>                  
                    <tr>
                        <td> <label>Title:</label></td>
                        <td>
                            <input type="text" name="title" value="<?php echo $result['title']; ?>" class="medium" />
                        </td>
                    </tr>
                   <tr>
                                <td>
                                    <label>Upload Image</label>
                                </td>
                                <td>
                                    <img src="<?php echo $result['image']; ?>" alt="image" height="80px" width="200px"><br/>
                                    <input type="file" name="image" />
                                </td>
                            </tr>
                    <tr> 
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Update" />
                        </td>
                    </tr>
                </table>

            <?php }
        }                        
        ?>
    </form>
</div>
</div>
</div>
<?php include "inc/footer.php"; ?>