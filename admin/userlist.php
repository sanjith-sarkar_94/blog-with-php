<?php include "inc/header.php";
include "inc/sidebar.php"; 
?>
<div class="grid_10">
	<div class="box round first grid">
		<h2>Category List</h2>
		<div class="block">       
			<?php 
			if (isset($_GET['catid'])) {	
				$catid = $_GET['catid'];
				$sql = "DELETE FROM tbl_category WHERE id = '$catid'";
				$delCatid = $db->delete($sql);
				if ($delCatid) {
					echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Success!!</strong> Thank You, Category Deleted Successfully</span></div>";			
				}else{
					echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Category Not Deleted</span></div>";
				}
			}
			?> 
			<table class="data display datatable" id="example">
				<thead>
					<tr>
						<th>Serial No.</th>
						<th>UserName</th>
						<th>Email</th>
						<th>Role</th>					
						<?php if (Session::get('userrole') == 0) {?>
							<th>Action</th>	
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<?php 
					$sql = "SELECT * FROM tbl_user order by id ASC";
					$catlist = $db->select($sql);
					if ($catlist) {
						while ($result = $catlist->fetch_assoc()) { ?>
							<tr class="odd gradeX">
								<td><?php echo $result['id']; ?></td>
								<td><?php echo $result['username']; ?></td>
								<td><?php echo $result['email']; ?></td>
								<td><?php echo $result['role']; ?></td>
								<?php if (Session::get('userrole') == 0) {?>
									<td><a class="btn btn-primary" href="editcat.php?catid=<?php echo $result['id']; ?>">Edit</a> || <a onclick="return confirm('are you sure to delete!')" class="btn btn-danger" href="?catid=<?php echo $result['id']; ?>">Delete</a></td>
								<?php } ?>
							</tr>
						<?php }
					}else{
						echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, catlist not found</span></div>";
					}
					?>


				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="clear">
</div>
</div>

<!-- END: load jquery -->
<script type="text/javascript" src="js/table/table.js"></script>
<script src="js/setup.js" type="text/javascript"></script>
<script type="text/javascript">

	$(document).ready(function () {
		setupLeftMenu();

		$('.datatable').dataTable();
		setSidebarHeight();


	});
</script>


<?php include "inc/footer.php"; ?>