﻿<?php include "inc/header.php";
include "inc/sidebar.php"; 

      // update social site

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['update'])) {
  $fb = $fm->validation($_POST['facebook']);
  $tw = $fm->validation($_POST['twitter']);
  $ln = $fm->validation($_POST['linkedin']);
  $gp = $fm->validation($_POST['googleplus']);

  $fb = mysqli_real_escape_string($db->link, $fb);
  $tw = mysqli_real_escape_string($db->link, $tw);
  $ln = mysqli_real_escape_string($db->link, $ln);
  $gp = mysqli_real_escape_string($db->link, $gp);

  if ($fb == "" || $tw == "" || $ln == "" || $gp == "") {
    echo "<span>Field must not empty!!</span>";
}else{

  $sql = "UPDATE tbl_social SET 
  fb = '$fb',
  tw = '$tw',
  ln = '$ln',
  gp = '$gp'
  WHERE id = '1'";

  $socialInsert = $db->update($sql);
  if ($socialInsert) {
    echo "<div style='text-align:center;'><span class='btn bg-success;'>Social Updated Successfully.
    </span></div>";
}else {
    echo "<div style='text-align:center;'><span class='btn bg-success;'>Social Not Updated Successfully.
    </span></div>";
}
}
}
?>
<div class="grid_10">

    <div class="box round first grid">
        <h2>Update Social Media</h2>
        <div class="block">
            <?php $sql = "SELECT * FROM tbl_social";
            $sociallist = $db->select($sql);
            if ($sociallist) {
                while($result = $sociallist->fetch_assoc()){ ?>
                 <form action="" method="POST">
                    <table class="form">                    
                        <tr>
                            <td>
                                <label>Facebook</label>
                            </td>
                            <td>
                                <input type="text" name="facebook" value="<?php echo $result['fb'] ?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Twitter</label>
                            </td>
                            <td>
                                <input type="text" name="twitter" value="<?php echo $result['tw'] ?>" class="medium" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <label>LinkedIn</label>
                            </td>
                            <td>
                                <input type="text" name="linkedin" value="<?php echo $result['ln'] ?>" class="medium" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <label>Google Plus</label>
                            </td>
                            <td>
                                <input type="text" name="googleplus" value="<?php echo $result['gp'] ?>" class="medium" />
                            </td>
                        </tr>
                        
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="update" Value="Update" />
                            </td>
                        </tr>
                    </table>
                </form>
            <?php   }
        }
        ?>                            
    </div>
</div>
</div>

<?php include "inc/footer.php"; ?>