<?php include "inc/header.php";
include "inc/sidebar.php"; 

// get editid from post page

if (!isset($_GET['editid']) || $_GET['editid'] == NULL) {
	echo "<script>window.location = 'postlist.php';</script>";
}else{
	$editid = $_GET['editid'];
}

// update post 

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['update'])) {

    $title = $fm->validation($_POST['title']);
    $cat = $fm->validation($_POST['select']);
    $body = $fm->validation($_POST['body']);
    $tags = $fm->validation($_POST['tags']);
    $author = $fm->validation($_POST['author']);
    $userid = $fm->validation($_POST['userid']);

    $title = mysqli_real_escape_string($db->link, $title);
    $cat = mysqli_real_escape_string($db->link, $cat);
    $body = mysqli_real_escape_string($db->link, $body);
    $tags = mysqli_real_escape_string($db->link, $tags);
    $author = mysqli_real_escape_string($db->link, $author);
    $author = mysqli_real_escape_string($db->link, $userid);

	$permited  = array('jpg', 'jpeg', 'png', 'gif');
	$file_name = $_FILES['image']['name'];
	$file_size = $_FILES['image']['size'];
	$file_temp = $_FILES['image']['tmp_name'];

	$div = explode('.', $file_name);
	$file_ext = strtolower(end($div));
	$unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
	$uploaded_image = 'upload/'.$unique_image;


	if ($title == "" || $cat == "" || $body == "" || $tags == "" || $author == "") {
		echo "<span class='error'>Field must no empty!</span>";
	}else{
		if (!empty($file_name)) {
			if ($file_size >1048567) {
				echo "<span class='error'>Image Size should be less then 1MB!</span>";
			} elseif (in_array($file_ext, $permited) === false) {
				echo "<span class='error'>You can upload only:-"
				.implode(', ', $permited)."</span>";
			} else{
				move_uploaded_file($file_temp, $uploaded_image);
				$query = "UPDATE tbl_post SET 
				title = '$title',
				cat = '$cat',
				body = '$body',
				tags = '$tags',
				author = '$author',
				image = '$uploaded_image',
				userid = '$userid'
				WHERE id = '$editid'
				";
				$inserted_rows = $db->update($query);

				if ($inserted_rows) {
					echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Updated Successfully.
					</span></div>";
				}else {
					echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Not Updated Successfully.
					</span></div>";
				}
			}
		}
		else{
			$query = "UPDATE tbl_post SET 
			title = '$title',
			cat = '$cat',
			body = '$body',
			tags = '$tags',
			author = '$author',
			userid = '$userid'
			WHERE id = '$editid'
			";
			$inserted_rows = $db->update($query);

			if ($inserted_rows) {
				echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Updated Successfully.
				</span></div>";
			}else {
				echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Not Updated Successfully.
				</span></div>";
			}
		}
	}
}
?>


<div class="grid_10">
	<div class="box round first grid">
		<h2 style="text-align: center;">Update Post</h2>
		<div class="block"> 
			<?php
			$sql = "SELECT * FROM tbl_post WHERE id = '$editid'";
			$editpost = $db->select($sql);
			if ($editpost) {
				while($postresult = $editpost->fetch_assoc()){ ?>
					<form action="" method="POST" enctype="multipart/form-data">
						<table class="form">
							<tr>
								<td>
									<label>Title</label>
								</td>
								<td>
									<input type="text" name="title" value="<?php echo $postresult['title']; ?>" class="medium" />
								</td>
							</tr>

							<tr>
								<td>
									<label>Category</label>
								</td>
								<td>
									<select id="select" name="select">
										<?php 
										$sql = "SELECT * FROM tbl_category";
										$catlist = $db->select($sql);
										if ($catlist) { 
											while($result = $catlist->fetch_assoc()){?>
												<option
												<?php if ($postresult['cat'] == $result['id']){ ?>
													selected = "selected"
												<?php } ?>
												value="<?php echo $result['id']; ?>"><?php echo $result['name']; ?></option>                         
											<?php }
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<label>Upload Image</label>
								</td>
								<td>
									<img src="<?php echo $postresult['image']; ?>" alt="image" height="80px" width="200px"><br/>
									<input type="file" name="image" />
								</td>
							</tr>
							<tr>
								<td>
									<label>Tags</label>
								</td>
								<td>
									<input type="text" name="tags" value="<?php echo $postresult['tags']; ?>" class="medium" />
								</td>
							</tr>
							<tr>
								<td style="vertical-align: top; padding-top: 9px;">
									<label>Content</label>
								</td>
								<td>

									<textarea name="body" class="tinymce"><?php echo $postresult['body']; ?>"</textarea>
								</td>
							</tr>
							<tr>
								<td>
									<label>Author</label>
								</td>
								<td>
									<input type="text" name="author" readonly value="<?php $user = Session::get('username'); echo $user; ?>" class="medium" />
									<input type="hidden" name="userid" readonly value="<?php $user = Session::get('userid'); echo $user; ?>" class="medium" />
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input type="submit" name="update" Value="Save" />
								</td>
							</tr>
						</table>
					</form>
				<?php }
			}
			?>              

		</div>
	</div>
</div>

<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		setupTinyMCE();
		setDatePicker('date-picker');
		$('input[type="checkbox"]').fancybutton();
		$('input[type="radio"]').fancybutton();
	});
</script>
<?php include "inc/footer.php"; ?>

</body>
</html>

