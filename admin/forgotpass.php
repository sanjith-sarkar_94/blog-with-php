<?php 
include "../lib/Session.php";
Session::init();
Session::checkLogin();
?>
<?php
include "../config/config.php";
include "../lib/Database.php";
include "../helpers/format.php";
$db = new Database();
$fm = new Format();

if (isset($_POST['searchmail'])) {
	$email = $fm->validation($_POST['email']);

	$email = mysqli_real_escape_string($db->link, $email);

	$sql = "SELECT * FROM tbl_user WHERE email = '$email'";
	$mailcheck = $db->select($sql);
	 if ($mailcheck == true) {
		echo "<div style='text-align:center;'><span class='btn btn-success'><strong>Success!!</strong> Thank You, Email found!!.</span></div>";
	}else{
		echo "<div style='text-align:center;'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Email not found!!.</span></div>";
	}
}
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title>Password Recovery</title>
	<link rel="stylesheet" type="text/css" href="css/stylelogin.css" media="screen" />
</head>
<body>
	<div class="container">
		<section id="content">
			<form action="" method="post">
				<h1>Password Recovery</h1>
				<div>
					<input type="text" placeholder="Enter Email...." required="" name="email" required />
				</div>
				<div>
					<input type="submit" name="searchmail" value="Check Mail" />
					<input type="submit" name="submit" value="Send Mail" />
				</div>
			</form><!-- form -->
			<div class="button">
				<a href="login.php">Login!</a>
			</div>
			<div class="button">
				<a href="#">Dynamic Live Project!</a>
			</div>
		</section><!-- content -->
	</div><!-- container -->
</body>
</html>