<?php include "inc/header.php";
include "inc/sidebar.php"; 


if (!isset($_GET['msgid']) || $_GET['msgid'] == NULL){
    echo "<script>window.location = 'inbox.php';</script>";
}else{
    $msgid = $_GET['msgid'];
}


if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {
   echo "<script>window.location = 'inbox.php';</script>";

}

if (isset($_GET['seenid'])) {
    $seenid = $_GET['seenid'];
    $sql = "UPDATE tbl_contact SET 
    status = '1'
    WHERE id = '$seenid'
    ";

    $msglist = $db->update($sql);
    if ($msglist) {
        echo "<div style='text-align:center;'><span class='btn bg-success;'>Message sent in the Seen box.</span></div>";
    }else{
        echo "<div style='text-align:center;'><span class='btn bg-success;'>Message does not send.</span></div>";
    }
}
?>

<div class="grid_10">

    <div class="box round first grid">
        <h2>View Message</h2>
        <div class="block">
            <?php
            $sql = "SELECT * FROM tbl_contact WHERE id = '$msgid'";
            $viewmsg = $db->select($sql);
            if ($viewmsg) {
                while($msgresult = $viewmsg->fetch_assoc()){ ?>               
                 <form action="" method="POST">
                    <table class="form">
                        <tr>
                            <td>
                                <label>Serial ID</label>
                            </td>
                            <td>
                                <input type="text" name="id" readonly class="medium" value="<?php echo $msgresult['id']; ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>First Name</label>
                            </td>
                            <td>
                                <input type="text" name="firstname" class="medium" value="<?php echo $msgresult['firstname']." ".$msgresult['lastname'] ; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Email</label>
                            </td>
                            <td>
                                <input type="email" name="email" class="medium" value="<?php echo $msgresult['email']; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Date</label>
                            </td>
                            <td>
                                <input type="text" name="date" readonly class="medium" value="<?php echo $fm->dateFormat($msgresult['date']); ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Body</label>
                            </td>
                            <td>
                                <input type="text" name="body" class="medium" value="<?php echo $msgresult['body']; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Back" />
                                <a href="replaymsg.php?msgid=<?php echo $msgresult['id']; ?>">Replay</a>||
                                <a onclick="return confirm('are you sure to move the seen box!')" href="?seenid=<?php echo $msgresult['id']; ?>">Seen</a>
                            </td>
                        </tr>
                    </table>
                </form>
            <?php }} ?>
        </div>
    </div>
</div>

<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php"; ?>

</body>
</html>

