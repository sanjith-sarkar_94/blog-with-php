<?php include "inc/header.php";
include "inc/sidebar.php"; 

// get session id

$userid = Session::get('userid');


// update profile


if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {

    $username = $fm->validation($_POST['username']);
    $password = $fm->validation($_POST['password']);
    $role = $fm->validation($_POST['role']);

    $username = mysqli_real_escape_string($db->link, $username);
    $password = mysqli_real_escape_string($db->link, $password);
    $role = mysqli_real_escape_string($db->link, $role);

    if ($username == "" || $password == "" || $role == "") {
        echo "<span class='error'>Field must no empty!</span>";
    }else{
        $sql = "UPDATE tbl_user SET 
        username = '$username',
        password = '$password',
        role = '$role'
        WHERE id = '$userid'";

        $userupd = $db->update($sql);
        if ($userupd) {
           echo "<div style='text-align:center;'><span class='btn bg-success;'>Profile Updated Successfully.
           </span></div>";
       }else {
           echo "<div style='text-align:center;'><span class='btn bg-success;'>Profile Not Updated Successfully.
           </span></div>";
       }
   }
}
?>

<div class="grid_10">

    <div class="box round first grid">
        <h2>Add New User</h2>
        <div class="block">   
           <?php
           $sql = "SELECT * FROM tbl_user INNER JOIN tbl_userrole ON tbl_user.id = tbl_userrole.id AND tbl_user.id = '$userid'";
           $userlist = $db->select($sql);
           if ($userlist) {
            while($userresult = $userlist->fetch_assoc()){ ?>            
             <form action="" method="POST">
                <table class="form">
                    <tr>
                        <td>
                            <label>Username</label>
                        </td>
                        <td>
                            <input type="text" name="username" value="<?php echo $userresult['username']; ?>" class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Password</label>
                        </td>
                        <td>
                            <input type="password" name="password" value="<?php echo $userresult['password']; ?>" class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Role</label>
                        </td>
                        <td>
                            <select id="select" name="role">
                             <option><?php echo $userresult['name'] ?></option>
                             <?php $sql = "SELECT * FROM tbl_userrole";
                             $userlist = $db->select($sql);
                             if ($userlist) {
                                while($userresult = $userlist->fetch_assoc()){ ?>
                                    <option><?php echo $userresult['name'] ?></option>
                                <?php }} ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Update" />
                        </td>
                    </tr>
                </table>
            </form>
        <?php }} ?>
    </div>
</div>
</div>

<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php"; ?>

</body>
</html>

