<?php include "inc/header.php";
include "inc/sidebar.php"; 


if (!isset($_GET['msgid']) || $_GET['msgid'] == NULL){
    echo "<script>window.location = 'inbox.php';</script>";
}else{
    $msgid = $_GET['msgid'];
}


if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {
    echo "<script>window.location = 'inbox.php';</script>"; 
}
?>

<div class="grid_10">

    <div class="box round first grid">
        <h2>View Message</h2>
        <div class="block">
        <?php
            $sql = "SELECT * FROM tbl_contact WHERE id = '$msgid'";
            $viewmsg = $db->select($sql);
            if ($viewmsg) {
                while($msgresult = $viewmsg->fetch_assoc()){ ?>               
           <form action="" method="POST">
            <table class="form">
                <tr>
                    <td>
                        <label>To</label>
                    </td>
                    <td>
                        <input type="email" readonly name="email" class="medium" value="<?php echo $msgresult['email']; ?>"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>From</label>
                    </td>
                    <td>
                        <input type="text" name="fromEmail" placeholder="Please enter your email" class="medium"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Subject</label>
                    </td>
                    <td>
                        <input type="email" name="email" placeholder="Please enter Subject" class="medium"/>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top; padding-top: 9px;">
                        <label>Message</label>
                    </td>
                    <td>
                        <textarea name="message" class="tinymce"></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" Value="Send" />
                    </td>
                </tr>
            </table>
        </form>
    <?php }} ?>
    </div>
</div>
</div>

<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php"; ?>

</body>
</html>

