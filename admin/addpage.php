<?php include "inc/header.php";
include "inc/sidebar.php"; 


// add post


if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {

    $name = $fm->validation($_POST['name']);
    $body = $fm->validation($_POST['body']);

    $name = mysqli_real_escape_string($db->link, $name);
    $body = mysqli_real_escape_string($db->link, $body);

    if ($name == "" || $body == "") {
        echo "<span class='error'>Field must no empty!</span>";
    }else{
        $query = "INSERT INTO tbl_page(name, body) VALUES('$name', '$body')";
        $inserted_rows = $db->insert($query);
        if ($inserted_rows) {
         echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Inserted Successfully.
         </span></div>";
     }else {
         echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Not Inserted Successfully.
         </span></div>";
     }
 }
}
?>

<div class="grid_10">

    <div class="box round first grid">
        <h2>Add New Post</h2>
        <div class="block">               
           <form action="" method="POST">
            <table class="form">
                <tr>
                    <td>
                        <label>Name</label>
                    </td>
                    <td>
                        <input type="text" name="name" placeholder="Enter Post Title..." class="medium" />
                    </td>
                </tr>
                
                <tr>
                    <td style="vertical-align: top; padding-top: 9px;">
                        <label>Content</label>
                    </td>
                    <td>
                        <textarea name="body" class="tinymce"></textarea>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" Value="Create" />
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
</div>

<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php"; ?>

</body>
</html>

