<?php include "inc/header.php";
include "inc/sidebar.php"; 
?>
<div class="grid_10">
	<div class="box round first grid">
		<h2>Category List</h2>
		<div class="block">       
			<?php 
			if (isset($_GET['delid'])) {	
				$slideid = $_GET['delid'];
				$sql = "DELETE FROM tbl_slider WHERE id = '$slideid'";
				$delSlideid = $db->delete($sql);
				if ($delSlideid) {
					echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Success!!</strong> Thank You, Category Deleted Successfully</span></div>";			
				}else{
					echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Category Not Deleted</span></div>";
				}
			}
			?> 
			<table class="data display datatable" id="example">
				<thead>
					<tr>
						<th>Serial No.</th>
						<th>Title</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$sql = "SELECT * FROM tbl_slider order by id ASC";
					$slidelist = $db->select($sql);
					if ($slidelist) {
						while ($result = $slidelist->fetch_assoc()) { ?>
							<tr class="odd gradeX">
								<td><?php echo $result['id']; ?></td>
								<td><?php echo $result['title']; ?></td>
								<td><img src="<?php echo $result['image'] ?>" height="40px" width="60px"></td>
								<td><a class="btn btn-primary" href="editslider.php?slideid=<?php echo $result['id']; ?>">Edit</a> || <a onclick="return confirm('are you sure to delete!')" class="btn btn-danger" href="?delid=<?php echo $result['id']; ?>">Delete</a></td>
							</tr>
						<?php }
					}else{
						echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Slidelist not found</span></div>";
					}
					?>


				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="clear">
</div>
</div>

<!-- END: load jquery -->
<script type="text/javascript" src="js/table/table.js"></script>
<script src="js/setup.js" type="text/javascript"></script>
<script type="text/javascript">

	$(document).ready(function () {
		setupLeftMenu();

		$('.datatable').dataTable();
		setSidebarHeight();


	});
</script>


<?php include "inc/footer.php"; ?>