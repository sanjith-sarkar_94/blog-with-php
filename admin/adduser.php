<?php include "inc/header.php";
include "inc/sidebar.php"; 

// block user to access adduser


if (!Session::get('userrole') == 0) {
   echo "<script>window.location = 'index.php';</script>";
}

// Find email


// add post


if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {

    $username = $fm->validation($_POST['username']);
    $email = $fm->validation($_POST['email']);
    $password = md5($fm->validation($_POST['password']));
    $role = $fm->validation($_POST['role']);

    $username = mysqli_real_escape_string($db->link, $username);
    $email = mysqli_real_escape_string($db->link, $email);
    $password = mysqli_real_escape_string($db->link, $password);
    $role = mysqli_real_escape_string($db->link, $role);

    $sql = "SELECT * FROM tbl_user WHERE email = '$email'";
    $result = $db->select($sql);


    if ($username == "" || $password == "" || $role == "" || $email == "") {
        echo "<span class='error'>Field must no empty!</span>";
    }elseif ($result == true) {
       echo "<div style='text-align:center;'><span class='btn bg-success;'>Email Already Exits.</span></div>";
    }else{

        $query = "INSERT INTO tbl_user(username, email, password, role) VALUES('$username', '$email', '$password', '$role')";
        $userInsert = $db->insert($query);
        if ($userInsert) {
         echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Inserted Successfully.
         </span></div>";
     }else {
         echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Not Inserted Successfully.
         </span></div>";
     }
 }
}
?>

<div class="grid_10">

    <div class="box round first grid">
        <h2>Add New User</h2>
        <div class="block">               
           <form action="" method="POST">
            <table class="form">
                <tr>
                    <td>
                        <label>Username</label>

                    </td>
                    <td>
                        <input type="text" name="username" placeholder="Enter Username..." class="medium" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Email</label>

                    </td>
                    <td>
                        <input type="email" name="email" placeholder="Enter Email..." class="medium" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Password</label>
                    </td>
                    <td>
                        <input type="password" name="password" placeholder="Enter Password..." class="medium" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Role</label>
                    </td>
                    <td>
                        <select id="select" name="role">
                            <option>Select User Role</option>
                            <option value="0">Admin</option>
                            <option value="1">Author</option>
                            <option value="2">Editor</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" name="submit" Value="Create" />
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
</div>

<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php"; ?>

</body>
</html>

