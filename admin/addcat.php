﻿<?php include "inc/header.php";
      include "inc/sidebar.php"; 
?>
        <div class="grid_10">
		
            <div class="box round first grid">
                <h2>Add New Category</h2>
               <div class="block copyblock"> 
                <?php
                if ($_SERVER['REQUEST_METHOD'] == "POST") {
                     $name = $_POST['name'];
                     $name = mysqli_real_escape_string($db->link, $name);
                     if (empty($name)) {
                         echo "<span>Field must not empty!!</span>";
                     }else{
                        $sql = "INSERT INTO tbl_category (name) VALUES('$name')";
                        $insertCat = $db->insert($sql);
                        if ($insertCat) {
                            echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Success!!</strong> Thank You, Category Inserted Successfully</span></div>";
                        }else{
                            echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, Category Not Inserted</span></div>";
                        }
                     }
                 } 

                 ?>
                 <form action="" method="POST">
                    <table class="form">					
                        <tr>
                            <td>
                                <input type="text" name="name" placeholder="Enter Category Name..." class="medium" />
                            </td>
                        </tr>
						<tr> 
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
        
        <?php include "inc/footer.php"; ?>

