<?php include "inc/header.php";
include "inc/sidebar.php"; 
?>
<style>
    .leftside{
     width: 70%;
     float: left;
 }
 .rightside{
     width: 30%;
     float: left;
 }
 .rightside img{
     width: 130px;
     height: 120px;
 }
</style>


<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Site Title and Description</h2>

        <?php  

        /// update title, slogan & logo

        if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['update'])) {

            $title = $fm->validation($_POST['title']);
            $slogan = $fm->validation($_POST['slogan']);
            
            $title = mysqli_real_escape_string($db->link, $title);
            $slogan = mysqli_real_escape_string($db->link, $slogan);


            $permited  = array('jpg', 'jpeg', 'png', 'gif');
            $file_name = $_FILES['logo']['name'];
            var_dump($file_name);
            $file_size = $_FILES['logo']['size'];
            var_dump($file_size);
            $file_temp = $_FILES['logo']['tmp_name'];
            var_dump($file_temp);

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $same_image = '$file_name'.'.'.$file_ext;
            $uploaded_image = 'upload/'.$same_image;


            if ($title == "" || $slogan == "") {
                echo "<span class='error'>Field must no empty!</span>";
            }else{
                if (!empty($file_name)) {
                    if ($file_size >1048567) {
                        echo "<span class='error'>Image Size should be less then 1MB!</span>";
                    } elseif (in_array($file_ext, $permited) === false) {
                        echo "<span class='error'>You can upload only:-"
                        .implode(', ', $permited)."</span>";
                    } else{
                        move_uploaded_file($file_temp, $uploaded_image);
                        $query = "UPDATE title_slogan SET 
                        title = '$title',
                        slogan = '$slogan',
                        logo = '$uploaded_image'
                        WHERE id = '1'
                        ";
                        $inserted_rows = $db->update($query);
                        var_dump($inserted_rows);
                        if ($inserted_rows) {
                            echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Updated Successfully.
                            </span></div>";
                        }else {
                            echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Not Updated Successfully.
                            </span></div>";
                        }
                    }
                }
                else{
                    $query = "UPDATE title_slogan SET 
                    title = '$title',
                    slogan = '$slogan'
                    WHERE id = '1'
                    ";
                    $inserted_rows = $db->update($query);

                    if ($inserted_rows) {
                        echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Updated Successfully.
                        </span></div>";
                    }else {
                        echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Not Updated Successfully.
                        </span></div>";
                    }
                }
            }
        }
        ?>



        <?php 
        $sql = "SELECT * FROM title_slogan WHERE id = 1";
        $updatelogo = $db->select($sql);
        if ($updatelogo) {
            while ($result = $updatelogo->fetch_assoc()) { ?>
                <div class="block sloginblock">               
                    <div class="leftside">
                        <form action="" method="POST">
                            <table class="form">                    
                                <tr>
                                    <td>
                                        <label>Website Title</label>
                                    </td>
                                    <td>
                                        <input type="text"  name="title" value="<?php echo $result['title']; ?>" class="medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Website Slogan</label>
                                    </td>
                                    <td>
                                        <input type="text" name="slogan" value="<?php echo $result['slogan']; ?>" class="medium" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <label>Upload Image</label>
                                    </td>
                                    <td>
                                        <input type="file" name="logo" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <input type="submit" name="update" Value="update" />
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div class="rightside">
                        <img src="<?php echo $result['logo']; ?>" alt="logo">
                    </div>
                </div>
            <?php }
        } ?>
    </div>
</div>


<?php include "inc/footer.php"; ?>