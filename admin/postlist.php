﻿<?php include "inc/header.php";
      include "inc/sidebar.php"; 
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Post List</h2>
                <div class="block">  
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial</th>
							<th>Post Title</th>
							<th>Description</th>
							<th>Category</th>
							<th>Author</th>
							<th>Tags</th>
							<th>UserId</th>
							<th>Date</th>
							<th>Image</th>							
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
					$sql = "SELECT tbl_post.*, tbl_category.name FROM tbl_post INNER JOIN tbl_category ON tbl_post.cat = tbl_category.id ORDER By tbl_post.title DESC";
					$poslist = $db->select($sql);
					if ($poslist) {
						while ($result = $poslist->fetch_assoc()) { ?>
							<tr class="odd gradeX">
								<td><?php echo $result['id']; ?></td>
								<td><?php echo $fm->textShorten($result['title'], 25); ?></td>
								<td><?php echo $fm->textShorten($result['body'], 50); ?></td>						
								<td><?php echo $result['name']; ?></td>
								<td><?php echo $result['author']; ?></td>
								<td><?php echo $result['tags']; ?></td>								
								<td><?php echo $result['userid']; ?></td>
								<td><?php echo $fm->dateFormat($result['date']); ?></td>
								<td class="text-align: center;"><img src="<?php echo $result['image']; ?>" height="60px" width="60px"></td>
								
								<td width="17%">
									<a class="btn btn-primary" href="viewpost.php?editid=<?php echo $result['id']; ?>">View</a>
									
									<?php if (Session::get('userid') == $result['userid'] || Session::get('userrole') == 0) { ?>

										||<a class="btn btn-primary" href="editpost.php?editid=<?php echo $result['id']; ?>">Edit</a>||<a onclick="return confirm('are you sure to delete!')" class="btn btn-danger" href="deletepost.php?deleteid=<?php echo $result['id']; ?>">Delete</a>
								<?php	} ?>
									</td>
							</tr>
						<?php }
					}else{
						echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, catlist not found</span></div>";
					}
					?>
					</tbody>
				</table>
	
               </div>
            </div>
        </div>

	<script type="text/javascript">
        $(document).ready(function () {
            setupLeftMenu();
            $('.datatable').dataTable();
			setSidebarHeight();
        });
    </script>
   
<?php include "inc/footer.php"; ?>
