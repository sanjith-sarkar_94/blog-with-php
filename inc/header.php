<?php
include "config/config.php";
include "lib/Database.php";
include "helpers/format.php";
$db = new Database();
$fm = new Format();
?>

<!DOCTYPE html>
<html>
<head>
	<?php 
	if (isset($_GET['pageid'])) {
		$pageid = $_GET['pageid'];
		$sql = "SELECT * FROM tbl_page WHERE id = '$pageid'";
		$posts = $db->select($sql);
		if ($posts) {
			while($result = $posts->fetch_assoc()){ ?> 
				<title><?php echo $result['name']; ?> - <?php echo TITLE; ?></title>
			<?php   }
		}
	} elseif(isset($_GET['id'])){
		$postid = $_GET['id'];
		$sql = "SELECT * FROM tbl_post WHERE id = '$postid'";
		$editpost = $db->select($sql);
		if ($editpost) {
			while($postresult = $editpost->fetch_assoc()){ ?>
				<title><?php echo $postresult['title']; ?> - <?php echo TITLE; ?></title>
			<?php 	}
		}
	}else{ ?>
		<title><?php echo $fm->title(); ?> - <?php echo TITLE; ?></title>
	<?php }
	?>

	<meta name="language" content="English">
	<meta name="description" content="It is a website about education">
	<?php 
	if (isset($_GET['id'])) {
		$keywords = $_GET['id'];
		$sql = "SELECT * FROM tbl_post WHERE id = '$keywords'";
		$postKey = $db->select($sql);
		if ($postKey) {
			while($result = $postKey->fetch_assoc()){ ?> 
				<meta name="keywords" content="<?php echo $result['tags']; ?>">
			<?php } }else{ ?>
			<meta name="keywords" content="<?php echo KEYWORDS; ?>" >
		<?php
	}
} 
?>
<meta name="author" content="Sanjith">
<link rel="stylesheet" href="font-awesome-4.5.0/css/font-awesome.css">	
<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="style.css">
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery.nivo.slider.js" type="text/javascript"></script>

<script type="text/javascript">
	$(window).load(function() {
		$('#slider').nivoSlider({
			effect:'random',
			slices:10,
			animSpeed:500,
			pauseTime:5000,
		startSlide:0, //Set starting Slide (0 index)
		directionNav:false,
		directionNavHide:false, //Only show on hover
		controlNav:false, //1,2,3...
		controlNavThumbs:false, //Use thumbnails for Control Nav
		pauseOnHover:true, //Stop animation while hovering
		manualAdvance:false, //Force manual transitions
		captionOpacity:0.8, //Universal caption opacity
		beforeChange: function(){},
		afterChange: function(){},
		slideshowEnd: function(){} //Triggers after all slides have been shown
	});
	});
</script>
</head>

<body>
	<div class="headersection templete clear">
		<a href="index.php">
			<div class="logo">
				<?php  $sql = "SELECT * FROM title_slogan WHERE id = 1";
				$updatelogo = $db->select($sql);
				if ($updatelogo) {
					while ($result = $updatelogo->fetch_assoc()) { ?>
						<img src="admin/<?php echo $result['logo']; ?>" alt="Logo"/>
						<h2><?php echo $result['title']; ?></h2>
						<p><?php echo $result['slogan'] ?></p>
					<?php	}
				}?>
			</div>
		</a>
		<div class="social clear">
			<div class="icon clear">
				<a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
				<a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
				<a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
				<a href="#" target="_blank"><i class="fa fa-google-plus"></i></a>
			</div>
			<div class="searchbtn clear">
				<form action="search.php" method="get">
					<input type="text" name="search" placeholder="Search keyword..."/>
					<input type="submit" name="submit" value="Search"/>
				</form>
			</div>
		</div>
	</div>
	<div class="navsection templete">
		<ul>
			<?php 
			$path = $_SERVER['SCRIPT_FILENAME'];
			$currentpage = basename($path, '.php');
			?>
			<li><a
				<?php if ($currentpage == 'index') {
					echo 'id = "active"';
				} ?> href="index.php">Home</a></li>
				<?php $sql = "SELECT * FROM tbl_page";
				$posts = $db->select($sql);
				if ($posts) {
					while($result = $posts->fetch_assoc()){ ?>
						<li><a 
							<?php if (isset($_GET['pageid']) && $_GET['pageid'] == $result['id']){
								echo 'id = "active"';
							} ?>
							href="page.php?pageid=<?php echo $result['id']; ?>"><?php echo $result['name'] ?></a></li>
						<?php   }
					}
					?>
					<li><a 
						<?php if ($currentpage == 'contact') {
							echo 'id = "active"';
						} ?>
						href="contact.php">Contact</a></li>
					</ul>
				</div>