<div class="sidebar clear">
	<div class="samesidebar clear">
		<h2>Categories</h2>
		<ul>
			<?php 
			$sql = "SELECT * FROM tbl_category";
			$catlist = $db->select($sql);
			if ($catlist) {
				while ($result = $catlist->fetch_assoc()) { ?>
					<li><a href="posts.php?id=<?php echo $result['id']; ?>"><?php echo $result['name']; ?></a></li>
				<?php }
			}else{
				echo "<div style='text-align:center'><span class='btn btn-danger'><strong>Error!!</strong> Sorry, catlist not found</span></div>";
			}
			?>					
		</ul>
	</div>

	<div class="samesidebar clear">
		<h2>Latest articles</h2>
		<?php 
		$sql = "SELECT * FROM tbl_post";
		$post = $db->select($sql);
		if ($post) {
			while ($result = $post->fetch_assoc()) { 

				?>
				<div class="samepost clear">
					<h2><a href="post.php?id=<?php echo $result['id']; ?>"><?php echo $result['title']; ?></a></h2>
					<h4><?php echo $fm->dateFormat($result['date']); ?>, By <a href="#"><?php echo $result['author']; ?></a></h4>
					<a href="#"><img src="admin/<?php echo $result['image']?>" width="20px" height="50px" alt="post image"/></a>
					
					<?php echo $fm->textShorten($result['body'], 100); ?>

					<div class="readmore clear">
						<a href="post.php?id=<?php echo $result['id']; ?>">Read More</a>
					</div>
				</div>
			<?php } ?>
		<?php } else { header("Location: 404.php");}?>

	</div>

</div>
</div>
