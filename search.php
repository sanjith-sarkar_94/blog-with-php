<?php 
include "inc/header.php"; 
include "inc/slider.php";


/// Search

if (!isset($_GET['search']) && $_GET['search'] == NULL) {
	header("404.php");
}else{
	$search = $_GET['search'];
}


// pagination

$per_page = 3;
if (isset($_GET['page'])) {
	$page = $_GET['page'];
}else{
	$page = 1;
}

$start_from = ($page-1) * $per_page;
?>

</div>
<div class="contentsection contemplete clear">
	<div class="maincontent clear">
		<?php 
		$sql = "SELECT * FROM tbl_post WHERE title LIKE '%$search%' OR body LIKE '%$search%' OR author LIKE '%$search%' OR tags LIKE '%$search%' LIMIT $start_from, $per_page";
        $post = $db->select($sql);
		if ($post) {
			while ($result = $post->fetch_assoc()) { 

				?>
				<div class="samepost clear">
					<h2><a href="post.php?id=<?php echo $result['id']; ?>"><?php echo $result['title']; ?></a></h2>
					<h4><?php echo $fm->dateFormat($result['date']); ?>, By <a href="#"><?php echo $result['author']; ?></a></h4>
					<a href="#"><img src="admin/<?php echo $result['image'] ?>" alt="post image"/></a>
					
					<?php echo $fm->textShorten($result['body']); ?>

					<div class="readmore clear">
						<a href="post.php?id=<?php echo $result['id']; ?>">Read More</a>
					</div>
				</div>
			<?php } ?> <!-- End while loop -->

			<!----- Pagination ----------->



			<?php 
			//$sql = "SELECT * FROM tbl_post";
			$sql = "SELECT * FROM tbl_post WHERE title LIKE '%$search%' OR body LIKE '%$search%' OR author LIKE '%$search%' OR tags LIKE '%$search%'";
			$select_row = $db->select($sql);
			$num_rows = mysqli_num_rows($select_row);
			$total_pages = ceil($num_rows/$per_page);

			echo "<span class='pagination'><a href='index.php?page=1'>".'First Page'."</a>";
			for ($i=1; $i <= $total_pages; $i++) { 
					echo "<a href='index.php?page=$i'>".$i."</a>";
			}

			echo "<a href='index.php?page=$total_pages'>Last Page</a></span>";
			?>


		<?php } else { ?>
			<h3>Search Query not found!!</h3>
		<?php }?>


	</div>


	<?php include "inc/sidebar.php";?>


	<?php include "inc/footer.php";?>