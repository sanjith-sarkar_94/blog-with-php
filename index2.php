<?php include "inc/header.php"; 
include "inc/slider.php";
include "helpers/format.php";
$db = new Database();
$fm = new Format();

?>


<?php 

/// Pagination
if (isset($_GET['page'])) {
	$page = $_GET['page'];
}else{
	$page = 1;
}

$per_page = 3;
$start_from = ($page-1)*$per_page;

?>

</div>
<div class="contentsection contemplete clear">
	<div class="maincontent clear">
		<?php 
		$sql = "SELECT * FROM tbl_post LIMIT $start_from, $per_page";
        $post = $db->select($sql);
		if ($post) {
			while ($result = $post->fetch_assoc()) { 

				?>
				<div class="samepost clear">
					<h2><a href="post.php?id=<?php echo $result['id']; ?>"><?php echo $result['title']; ?></a></h2>
					<h4><?php echo $fm->dateFormat($result['date']); ?>, By <a href="#"><?php echo $result['author']; ?></a></h4>
					<a href="#"><img src="admin/upload/<?php echo $result['image'] ?>" alt="post image"/></a>
					
					<?php echo $fm->textShorten($result['body']); ?>

					<div class="readmore clear">
						<a href="post.php?id=<?php echo $result['id']; ?>">Read More</a>
					</div>
				</div>
			<?php } ?> <!-- End while loop -->

			<!----- Pagination ----------->


			<?php 
			$sql = "SELECT * FROM tbl_post";
			$post = $db->select($sql);
			$total_rows = mysqli_num_rows($post);
			$total_pages = ceil($total_rows/$per_page);

			echo "<span class='pagination'><a href='index2.php?page=1'>".'First Page'."</a>";
			for ($i=1; $i <= $total_pages; $i++) { 
					echo "<a href='index2.php?page=$i'>".$i."</a>";
			}

			echo "<a href='index2.php?page=$total_pages'>Last Page</a></span>";
			?>


		<?php } else { header("Location: 404.php");}?>




		<!-- <div class="samepost clear">
			<h2><a href="">Our post title here</a></h2>
			<h4>April 10, 2016, 12:30 PM, By <a href="#">Delowar</a></h4>
			<img src="images/post2.png" alt="post image"/>
			<p>
				Some text will be go here. Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here. Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.
			</p>
			<div class="readmore clear">
				<a href="post.php">Read More</a>
			</div>
		</div>
		<div class="samepost clear">
			<h2>Our post title here</h2>
			<h4>April 10, 2016, 12:30 PM, By <a href="#">Delowar</a></h4>
			<img src="images/post1.jpg" alt="post image"/>
			<p>
				Some text will be go here. Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here. Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.Some text will be go here.
			</p>
			<div class="readmore clear">
				<a href="post.php">Read More</a>
			</div>
		</div> -->

	</div>


	<?php include "inc/sidebar.php";?>


	<?php include "inc/footer.php";?>