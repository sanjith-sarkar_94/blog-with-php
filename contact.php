<?php include "inc/header.php";?>

<?php if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submit'])) {
	$firstname = $fm->validation($_POST['firstname']);
	$lastname = $fm->validation($_POST['lastname']);
	$email = $fm->validation($_POST['email']);
	$body = $fm->validation($_POST['body']);

	$firstname = mysqli_real_escape_string($db->link, $firstname);
	$lastname = mysqli_real_escape_string($db->link, $lastname);
	$email = mysqli_real_escape_string($db->link, $email);
	$body = mysqli_real_escape_string($db->link, $body);


	$errorf = "";
	$errorl = "";
	$errore = "";
	$errorb = "";

	if (empty($firstname)) {
		$errorf = "Field must not empty!";
	}if(empty($lastname)){
		$errorl = "Field must not empty!";
	}if(empty($email)){
		$errore = "Field must not empty!";
	}if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$errore = "Invalid Email Address!";
	}if(empty($body)){
		$errorb = "Field must not empty!";
	}else{
		$msg = "OK";
	}

	$query = "INSERT INTO tbl_contact(firstname, lastname, email, body) VALUES('$firstname', '$lastname', '$email', '$body')";
	$inserted_rows = $db->insert($query);
	if ($inserted_rows) {
		echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Inserted Successfully.
		</span></div>";
	}else {
		echo "<div style='text-align:center;'><span class='btn bg-success;'>Post Not Inserted Successfully.
		</span></div>";
	}

}	

?>
<div class="contentsection contemplete clear">
	<div class="maincontent clear">
		<div class="about">
			<h2>Contact us</h2>
<!-- 			<?php 
			if (isset($error)) {
				echo "<span style='color: red'> $error </span>";
			}

			if (isset($msg)) {
				echo "<span style='color: red'> $error </span>";
			}
		?> -->
		<form action="" method="post">
			<table>
				<tr>
					<td>Your First Name:</td>
					<td>
						<?php if (isset($errorf)) {
							echo "<span style='color: red; display: block;'> $errorf </span>";
						} ?>
						<input type="text" name="firstname" placeholder="Enter first name"/>
					</td>
				</tr>
				<tr>
					<td>Your Last Name:</td>
					<td>
						<?php if (isset($errorl)) {
							echo "<span style='color: red; display: block;'> $errorl </span>";
						} ?>
						<input type="text" name="lastname" placeholder="Enter Last name"/>
					</td>
				</tr>

				<tr>
					<td>Your Email Address:</td>
					<td>
						<?php if (isset($errore)) {
							echo "<span style='color: red; display: block;'> $errore </span>";
						} ?>
						<input type="email" name="email" placeholder="Enter Email Address"/>
					</td>
				</tr>
				<tr>
					<td>Your Message:</td>
					<td>
						<?php if (isset($errorb)) {
							echo "<span style='color: red; display: block;'> $errorb </span>";
						} ?>
						<textarea name="body"></textarea>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" name="submit" value="Submit"/>
					</td>
				</tr>
			</table>
			<form>				
			</div>

		</div>

		<?php include "inc/sidebar.php"; 
		include "inc/footer.php";
	?>